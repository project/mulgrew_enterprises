<?php
// $Id: template.php,v 1.7 2010/11/14 01:41:18 danprobo Exp $
function me_preprocess_page(&$vars) {
 // Generate menu tree from source of Primary Links
 //$vars['main_menu_tree'] = menu_tree(variable_get('menu_main_menu_source','main-menu'));
 // Generate menu tree from source of Secondary Links
 //$vars['secondary_menu_tree'] = menu_tree(variable_get('menu_secondary_menu_source','secondary-menu'));
 // Render menu tree from main-menu
  $menu_tree = menu_tree('main-menu');
  $vars['main_menu_tree'] = render($menu_tree);
}

function simpler_page_class($sidebar_first, $sidebar_second) {
  if ($sidebar_first && $sidebar_second) {
    $class = 'sidebars-2';
    $id = 'sidebar-side-2';  
  }
  else if ($sidebar_first || $sidebar_second) {
    $class = 'sidebars-1';
    $id = 'sidebar-side-1';
  }

  if(isset($id)) {
    print ' id="'. $id .'"';
  }
  
   if(isset($class)) {
      print ' class="'. $class .'"';
  }

}

function simpler_preprocess_html(&$variables) {
  drupal_add_css(path_to_theme() . '/style.ie6.css',
  array('group' => CSS_THEME, 'browsers' => 
  array('IE' => 'IE 6', '!IE' => FALSE), 'preprocess' => FALSE));
}

function me_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];

  // Remove 'Home'.
  if (is_array($breadcrumb)) {
    array_shift($breadcrumb);
  }
  if (!empty($breadcrumb)) {
    $breadcrumb_separator = ' > ';
    $breadcrumb_str = implode($breadcrumb_separator, $breadcrumb);
    $breadcrumb_str .= $breadcrumb_separator;
    $out = '<div class="breadcrumb">' . $breadcrumb_str . '</div>';
    return $out;
  }
  return '';
}
