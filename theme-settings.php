<?php

function me_form_system_theme_settings_alter(&$form, $form_state) {
  // Breadcrumb.
  $form['misc'] = array(
    '#type' => 'fieldset',
    '#title' => t('Miscellaneous settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['misc']['display_breadcrumb'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display breadcrumb'),
    '#default_value' => theme_get_setting('display_breadcrumb'),
    '#description' => t('Check here if you want to display breadcrumb.'),
  );
}
